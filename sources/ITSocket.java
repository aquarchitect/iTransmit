package interactiontransmitter;

import processing.core.*;
import processing.data.*;
import processing.net.*;

import java.util.HashMap;
import java.awt.Color;

public class ITSocket {

    public int maxUsers = 1;
    public ITDrawable delegate;

    private ITServer server;
    private PApplet applet;

    private HashMap<String, ITUser> users = new HashMap<String, ITUser>();

    public ITSocket(PApplet applet) {
        this.server = new ITServer(applet, 9000);
        this.applet = applet;
    }

    public int numberOfActiveUser() {
        // number of active users
        return 0;
    }

    public void displayHostDescription() {
        if (delegate == null) { return; }
        String host = server.getHost();
        delegate.drawHost(host);
    }

    public void checkForClient() {
        Client client = server.available();
        if (client == null) { return; }

        String data = client.readString();
        if (data == null) { return; }

        String ip = client.ip();
        ITUser user = users.get(ip);

        if (user == null) {
            user = new ITUser(client);
            user.id = users.size();
            users.put(ip, user);
        }

        parseDataForUser(user, data);
        checkForClient();
    }

    private void parseDataForUser(ITUser user, String data) {
        JSONObject json = null;
        try {
            json = applet.parseJSONObject(data);
        } finally {
            if (json == null) { return; }

            ITSize size = ITSize.sizeFromJSON(json);
            if (size != null) { user.deviceSize = size; }

            if (delegate == null) { return; }

            ITCoord coord = ITCoord.coordFromJSON(json);
            if (coord != null) {
                delegate.userDidMoveToCoord(user, coord);
            }
        }
    }
}
