package interactiontransmitter;

import processing.core.*;
import processing.net.*;

public class ITServer extends Server {

    private int port;

    public ITServer(PApplet applet, int port) {
        super(applet, port);
        this.port = port;
    }

    public String getHost() {
        return this.ip() + ":" + port;
    }
}
