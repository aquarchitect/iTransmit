package interactiontransmitter;

public interface ITDrawable {

    public void drawHost(String host);

    public void userDidMoveToCoord(ITUser user, ITCoord coord);
    // public void userDidTranslate(ITClient client, Float distance);
    // public void userDidRotate(ITClient client, Float angle);
    // public void userDidScale(ITClient client, Float ratio);
}