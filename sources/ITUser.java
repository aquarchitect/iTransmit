package interactiontransmitter;

import processing.net.*;
import processing.core.*;

import java.awt.Color;

public class ITUser {

    public Color clr = Color.black;
    public int id = 0;
    public ITSize deviceSize;

    private Client client;

    public ITUser(Client client) {
        this.client = client;
    }
}
