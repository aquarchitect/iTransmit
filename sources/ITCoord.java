package interactiontransmitter;

import processing.data.*;

public class ITCoord {

    public float x;
    public float y;
    public float z;

    public ITCoord(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public ITCoord(float x, float y) {
        this(x, y, 0);
    }

    public String getDescription() {
        return "x: " + x + ", y: " + y + ", z:" + z;
    }

    public static ITCoord coordFromJSON(JSONObject json) {
        JSONObject object = null;
        try {
            object = json.getJSONObject("coord");
        } finally {
            if (object == null) { return null; }

            float x = object.getFloat("x");
            float y = object.getFloat("y");
            float z = object.getFloat("z");
            return new ITCoord(x, y, z);
        }
    }
}
