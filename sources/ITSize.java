package interactiontransmitter;

import processing.data.*;

public class ITSize {

    public int width;
    public int height;

    public ITSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public String getDescription() {
        return "width: " + width + ", height: " + height;
    }

    public static ITSize sizeFromJSON(JSONObject json) {
        JSONObject object = null;
        try {
            object = json.getJSONObject("size");
        } finally {
            if (object == null) { return null; }

            int width = object.getInt("w");
            int height = object.getInt("h");
            return new ITSize(width, height);
        }
    }
}
