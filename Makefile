PACKAGE = interactiontransmitter
EXTERNAL = /Applications/Processing.app/Contents/Java

default: $(SOURCE)
	@ # compile into package
	@ javac -cp $(EXTERNAL)/core.jar:$(EXTERNAL)/modes/java/libraries/net/library/net.jar -d . sources/*.java
	@ $(MAKE) jar

jar: $(PACKAGE)/
	@ jar cfv library/$(PACKAGE).jar $(PACKAGE)
	@ $(MAKE) clean

clean: $(PACKAGE)/
	@ rm -r $(PACKAGE)/