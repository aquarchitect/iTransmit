import interactiontransmitter.*;
import processing.net.*;

ITSocket socket;

void setup() {
    size(300, 400);
    background(255);

    socket = new ITSocket(this);
    socket.delegate = new Drawing();
    socket.displayHostDescription();
}

void draw() {
    socket.checkForClient();
}

class Drawing implements ITDrawable {

    public void drawHost(String host) {
        fill(0);
        textSize(12);
        text(host, 5, 15);
    }

    public void userDidMoveToCoord(ITUser user, ITCoord coord) {
        int x = int(coord.x * width);
        int y = int(coord.y * height);

        fill(0);
        noStroke();
        ellipse(x , y, 10, 10);
    }
}